String.prototype.text2html = function () {
  return this.replace('&', '&amp;')
             .replace('<', '&lt;')
             .replace('"', '&quot;')
             .replace(/^\s+/g, '')
             .replace(/\s+$/g, '')
             .replace('\n', '<br />');
};

if (Drupal.jsEnabled) {
  $(document).ready(function () {
    var enabled = 0;
    var toggle = function () {
      enabled = 1 - enabled;
      $('input', this).attr('checked', enabled ? 'checked' : '');
    };
    $('<div id="field-spotter-toggle"><input type="checkbox" />Field spotter</div>')
    .appendTo($('body'))
    .click(toggle);
    $('span.field-spotter').each(function () {
      var text = '&lt;?php print drupal_render($'+ $(this).attr('formkey').text2html() +'); ?&gt;';
      var label = $('<span style="display: none" class="field-label field-tip">'+ text +'</span>').prependTo(this);
      
      $(this)
        .mouseover(function () { if (enabled) { $(label).show(); } })
        .mouseout(function () { if (enabled) { $(label).hide(); } });
    });
  });
}